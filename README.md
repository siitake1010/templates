# テンプレート集

自分で利用する各種ファイルのテンプレート（や参考資料）を集めた。

このファイルもテンプレートの一種。

## Getting Started / スタートガイド

プロジェクトを複製し、ローカル環境で実行し、開発や検証ができるまでの手順を説明する。
実際のシステムにプロジェクトをデプロイする方法については、デプロイ項目を参照してください。

### Prerequisites / 必要条件

プロジェクトを走らせるために、どんなソフトウェアが必要で、それらをどのようにインストールするか。

### Installing / インストール

動作する開発環境の構築方法を段階的に例示する。
このシステムが出力するデータの例や、簡単なデモを示して終わり。

## Running the tests / テストの実行

自動テストをどのように実行するかをここで説明する。

### Break down into end to end tests / e2e テストまで詳解する場合

何のためのテストでどうしてそれを行うのかを説明する。

### And coding style tests / コーディングスタイルのテスト

何のためのテストでどうしてそれを行うのかを説明する。

## Deployment / デプロイ

実際のシステムにデプロイするための補足的な説明を行う。

## Built With / 協働するシステムのリスト

## Contributing / コントリビューション

Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting pull requests to us.
私たちのコーディング規範とプルリクエストの手順についての詳細は CONTRIBUTING.md を参照してください。

## Authors / 著者

* Billie Thompson - Initial work / 原著者 - PurpleBooth

See also the list of contributors who participated in this project.
このプロジェクトへの貢献者のリストもご覧ください。

## License / ライセンス

sample.
This project is licensed under the MIT License - see the LICENSE.md file for details
このプロジェクトは MIT ライセンスの元にライセンスされています。 詳細は LICENSE.md をご覧ください。

## Acknowledgments / 謝辞

* Hat tip to anyone whose code was used / コードを書いた人への感謝
* Inspiration / 何からインスピレーションを得たか
* etc / その他

