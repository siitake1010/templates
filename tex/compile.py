#!/usr/bin/env python
# TODO: 引数の確認をするように変更する

import argparse
import subprocess
import pathlib

def make_arg_parser():
    args = argparse.ArgumentParser(description='tex ファイルのコンパイルスクリプト')

    args.add_argument(
        '-b', '--bibtex',
        default=False,
        help='using bibtex command, default: None (pbibtex,..)',
    )
    args.add_argument(
        '--tex',
        default='uplatex',
        help='using tex command, default: uplatex (lualatex,..)',
    )
    args.add_argument(
        '-f', '--file',
        default='main',
        help='file name',
    )
    args.add_argument(
        '--dvipdf',
        default='dvipdfmx',
        help='using dvi to pdf command, default: dvipdfmx (dvipdfmx,..)',
    )
    args.add_argument(
        '-n', '--number',
        type=int, default=3,
        help='compile times, default: 3',
    )

    return args


def run(cmd):
    print('[command]', *cmd, '...', end='')
    out = subprocess.run(cmd, capture_output=True)
    if out.returncode != 0:
        print('\n[ERROR]')
        print(out.stdout.decode('utf-8'), '\n')
        print(out.stderr.decode('utf-8'), '\n')
        return False
    else:
        print('Success!')
        return True


def compile_tex(args):
    tex = args.tex
    bib = args.bibtex
    n = args.number
    dvi = args.dvipdf
    file = pathlib.Path(args.file)
    f_name = file.stem

    if n <= 0:
        return

    if not run([tex, '-halt-on-error', f_name]):
        return -1

    if not bool(bib):
        n -= 1
    else:
        if not run([bib, f_name]):
            return -1

    for _ in range(n):
        if not run([tex, '-halt-on-error', f_name]):
            return -1

    if not run([dvi, f_name]):
        return -1
    return 0


def compile_luatex(args):
    tex = args.tex
    n = args.number
    file = pathlib.Path(args.file)
    f_name = file.stem

    if n <= 0:
        return
    for _ in range(n):
        if not run([tex, '--halt-on-error', f_name]):
            return -1
    return 0


def main():
    parser = make_arg_parser()
    args = parser.parse_args()

    tex = args.tex
    if 'lua' in tex:
        return compile_luatex(args)
    else:
        return compile_tex(args)


if __name__ == '__main__':
    main()
