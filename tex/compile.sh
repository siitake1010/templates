#!/bin/bash

CMD=uplatex
#CMD=lualatex
FILE=main

if [[ $1 && $1 =~ ^[0-9]+$ ]]; then
  for i in `seq 1 $1`; do
    $CMD $FILE
  done
elif [[ $1 ]]; then
  echo "Usage:"
  echo "    bash this_file [number (compile times)]"
else
  $CMD $FILE
fi
dvipdfmx $FILE
