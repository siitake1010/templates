# README

コマンドに関しては Linux を想定しています。
ですが、ただのテキストファイルですので環境さえ整えれば、
windows や overleaf 上でもコンパイルできるはずです。

`.latexmkrc` を用意したので下のコマンドでコンパイルする。

```bash
latexmk main.tex
```