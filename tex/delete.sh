#!/bin/bash

while getopts p OPT
do
    case $OPT in
        "p" ) FLAG="TRUE";;
        * ) echo "Usage: delete.sh [-p]" 1>&2
            exit 1 ;;
    esac
done

rm *.{aux,dvi,log,bbl,idx,blg,toc}
if [ "$FLAG" = "TRUE" ]; then
    rm *.pdf
fi
