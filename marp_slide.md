---
marp: true
---
<!-- size: 16:9 -->

<!-- global style -->
<style>
h1 {
    color: #1e50a2;
    font-size: 70px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateY(-130%) translateX(-50%)
}
h2 {
    color: #007bbb;
    font-size: 60px;
    position: absolute;
    top: 29%;
    left: 5%;
    transform: translateY(-130%) translateX(0%);
    border-bottom: solid 8px skyblue;
    width: 1000px;
}
section {
    font-size: 48px;;
    position: absolute;
    padding-top: 190px;
}
header, footer {
    color: #808080;
    font-size: 32px;
    left: 10%;
}
</style>

<style scoped>
section {
    font-size: 40px;
    text-align: center;
    padding-top: 350px;
}
header {
    left: 50%;
    transform: translateX(-50%);
}
footer {
    left: 50%;
    transform: translateX(-50%);
}
</style>

# タイトルページ

2019/09/24
しいたけ とりもち

<!-- _header: HEADER -->
<!-- _footer: FOOTER -->

---
## 一枚目のスライド

* foo
    * foo
    * foo
* foo
* foo
* foo

<!-- _header: HEADER -->
<!-- _footer: FOOTER -->

---
## 二枚目スライド

1. bar
    1. bar
    1. bar
1. bar