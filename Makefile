# 対象言語: c, c++
# ディレクトリ構造
# ./src/
# ./build/   (<= 自動的につくられる)
# ./Makefile
# ライブラリ: gl glew glfw3を使用

# 実行ファイルの名前
TARGET_EXEC ?= a.out

# ビルドディレクトリとソースディレクトリ
BUILD_DIR ?= ./build
SRC_DIRS ?= ./src

SRCS := $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c -or -name *.s)
#SRCS := $(shell find $(SRC_DIRS) -name *.cpp)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

# note
# 変数ETC_INCには、外部ライブラリをリンクする際に、
# そのライブラリをリンクするために必要なオプションを記載する。
# って参考にしたやつには説明してあった。
# ETC_INC は コンパイル時にも呼ばれる。
# 正直いらない。
# が、残しておいても問題ない。だから、残しておく。
ETC_INC := $()

# 上に加え、gitを利用する場合を想定し、".git"は探索範囲外としている。
#INC_DIRS := $(shell find $(SRC_DIRS) -type d -name '.git' -prune -o -type d  -print)
#INC_DIRS := $(shell find ./include -type d)
INC_DIRS := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

# リンカ用オプション
LDFLAGS := $(shell pkg-config --libs gl glew glfw3)

# コンパイルオプション
#ASFLAGS := 
#CFLAGS := -Wall -Wextra -g
CXXFLAGS := -Wall -Wextra -g

# プリプロセッサ用オプション
CPPFLAGS ?= $(INC_FLAGS) $(shell pkg-config --cflags gl glew glfw3) -MMD -MP

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CXX) $(OBJS) -o $@ $(LDFLAGS) $(ETC_INC)
#	$(CC) $(OBJS) -o $@ $(LDFLAGS) $(ETC_INC)

# c source
#$(BUILD_DIR)/%.c.o: %.c 
#	mkdir -p $(dir $@)
#	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@ $(ETC_INC)

# cpp source
$(BUILD_DIR)/%.cpp.o: %.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@ $(ETC_INC)

.PHONY: clean
clean:
	rm -r $(BUILD_DIR)/*

-include $(DEPS)
