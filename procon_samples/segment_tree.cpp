
template<typename Monoid>
struct SegmentTree {
    using Func = function<Monoid (Monoid, Monoid)>;
    int size;
    vector<Monoid> seg;

    const Func func;
    const Monoid iden;

    SegmentTree(int n, const Func f, const Monoid& identity)
        :func(f), iden(identity)
    {
        size = 1;
        while (size < n) size <<= 1;
        seg.assign(2 * size, identity);
    }

    void update(int k, const Monoid& x) {
        k += size;
        seg[k] = x;
        while (k >>= 1) {
            seg[k] = func(seg[2 * k], seg[2 * k + 1]);
        }
    }

    Monoid query(int a, int b) {
        Monoid L = iden, R = iden;
        for (a += size, b += size; a < b; a >>= 1, b >>= 1) {
            if (a & 1) L = func(L, seg[a++]);
            if (b & 1) R = func(seg[--b], R);
        }
        return func(L, R);
    }
};